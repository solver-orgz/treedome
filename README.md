<div align="center">
  <h1>Treedome</h1>

  <p>
    <img alt="development status" src="https://img.shields.io/badge/status-in%20development-yellow" />
    <a href="https://codeberg.org/solver-orgz/treedome/releases/latest"><img alt="Gitea Release" src="https://img.shields.io/gitea/v/release/solver-orgz/treedome?gitea_url=https%3A%2F%2Fcodeberg.org"></a>
    <a href="https://matrix.to/#/#treedome:matrix.org"><img alt="matrix chat" src="https://img.shields.io/matrix/treedome%3Amatrix.org" /></a>
    <a href="https://codeberg.org/solver-orgz/treedome/src/branch/master/LICENSE"><img alt="Static Badge" src="https://img.shields.io/badge/license-GNU_Affero_General_Public_License_v3.0_or_later-brightgreen"></a>
  </p>

  <p>
    A local-first, encrypted, note taking application with tree-like structures, all written and saved in rich text format.
    Supports <a href="https://www.youtube.com/watch?v=cC1CqyCN9Q0">Linux</a>, <a href="https://youtu.be/XxbJw8PrIkc?t=41">Windows</a>, and <a href="https://www.youtube.com/watch?v=jPiUN8fb3Og">a certain walled-garden operating system</a>.
  </p>
</div>

<p align="center">
  <img src="docs/media/treedome-open-document.png" width="45%"></img>
  <img src="docs/media/treedome-document.png" width="45%"></img>
  <img src="docs/media/treedome-escape-menu.png" width="45%"></img>
  <img src="docs/media/treedome-shortcut-menu.png" width="45%"></img>
</p>

<div align="center">
  <p>
    <a href="#disclaimer">Disclaimer</a> •
    <a href="#features">Features</a> •
    <a href="#must-read-docs">Must Read Docs</a> •
    <a href="#origin-and-namesake">Origin and Namesake</a> •
    <a href="https://matrix.to/#/#treedome:matrix.org">Join The Community</a>
  </p>
</div>

# Project Status

Alive and maintained! Currently pretty damn stable, but there are some features I want to implement. Such as:

1. linking other notes in the same document
2. using keyboard to navigate the tree
3. using context menu to manipulate notes in trees
4. selecting multiple notes in trees

The software will never be finished! But maybe soon I will bump it to 1.0, once I've made the logo of course.

# Features

Please note that this project is currently maintained by a single person only. Broadly speaking, these are the features that's included in the application.

- All notes are secured by password based encryption.
- Notes are stored in a tree like structure.
- Notes are able to be tagged.
- Own your data, nothing is online, everything is local.
- Notes are edited using modern rich text editor.
- Inserting images is supported.
- Quick access to notes using shortcut keys.

Planned for future releases:

- Custom keybinding
- Custom theme
- Custom zoom level
- Localisation

# Must Read Docs

- If you just want to use it, please read [Installation](./docs/user/installation.md)
- [Comparison with Similar Applications](./docs/user/comparison-with-similar-applications.md)
- [Development Setup](./docs/development/development-setup.md)
- [For First Time Contributor](./docs/development/first-time-contributor.md)
- [Encryption](./docs/development/encryption.md)
- [Release Standard Operating Procedure](./docs/standard-operating-procedure/release.md)

# Origin and Namesake

**Inspiration and Goals**

Treedome was born from the need for a robust note-taking application that prioritizes data security. Our main inspiration came from [CherryTree](https://github.com/giuspen/cherrytree), another notable note-taking solution. While CherryTree remains an actively developed project, its creator sought to address certain limitations and areas of improvement.

**Key Motivations**

The Treedome project aims to rectify the following issues:

- Encrypted notes are currently secondary features in existing solutions.
- Loading encrypted notes into memory can lead to performance bottlenecks when dealing with large binary files. This inefficiency impacts user experience, particularly when saving photos or other substantial data.
- Utilizing non-web technologies can limit the potential for seamless and modern user experiences.

This project serves as a platform for refining skills in various areas:

- Rust programming language
- TypeScript programming language
- Nix system administration
- Docker containerization

The creator also aims to incorporate cutting-edge cryptography technologies to ensure the utmost protection of sensitive information.

**Call to Action**

We invite you to explore Treedome and contribute your expertise towards its growth. Your participation is valued, and we look forward to collaborating with the community.

# Disclaimer

**Software Reliability**

While the developer actively uses and tests this application daily, no guarantees can be made regarding its stability. As with any software, imperfections are inevitable, and data corruption may occur. It is essential to regularly back up your notes to prevent data loss.

**Encryption**

The encryption used by this software has not undergone professional auditing, although the code is available for review in `src-tauri/src/app/crypto.rs`. Users are advised to examine the code themselves if they require assurance on this matter.

**Data Integrity**

Please note that changes to the data structure used to serialize and deserialize data from the database may occur before reaching version 1. This could potentially break existing functionality.

# License

Below is the license notice of Treedome. Any files that doesn't have the license notice MUST be assumed to use this license notice instead. Although not legally binding, you can read more about Libreware by visiting [this website](https://www.libreware.org/).

```
Treedome is libre software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
```

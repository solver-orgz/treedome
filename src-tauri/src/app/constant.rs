pub const TREEDOME_EXTENSION: &str = "note";
pub const TREEDOME_BACKUP_EXTENSION: &str = "bak.note";

/// Will be used for exporting note, either for changing password or the likes.
pub const TREEDOME_NEW_EXTENSION: &str = "new.note";
pub const TREEDOME_TITLE_DEFAULT: &str = "Untitled Note";

pub const WELCOMING_NOTE_TITLE: &str = "Welcome to Treedome!";
pub const WELCOMING_NOTE_TEXT_ONLY: &str = "What is Treedome? As you can see, it's a note taking desktop application. Everything that you've typed here will be stored in your hard disk securely! Meaning, if you've saved it using this application, no one can reasonably (thousands of years) see what's inside of your notes. Who is Treedome for? Everyone that has a secret really. Which means everyone, ever! Here, let me give you an example. Teenager that wants a digital diary which their parents shouldn't know about. Office worker with trade secrets and confidential notes for his eyes only. Super secret agent with super secret purpose doing for a super secret entity. Why use Treedome? So, you want me to convince you why you should use Treedome? It is free! You don't need to pay anyone a single cent to use this application in its full glory. For non-techies, this means no one will sue or take this program away from you. For the lawyers/techies, this software is licensed on AGPLv3. It is simple! Just create a document that's sitting in your hard drive and you're good to go! It is secure! Everything that you put in this note will always be protected using an cipher based on your password. For techies out there, we are using scrypt with 2^15 difficulty, use the result of the cipher as the private key for a stream cipher called XChaCha20. More detail on the encryption in the README of this project. No logins! We are also excited for our capitalistic overlord poaching every bit of private information from the social media. But not this application! Everything is offline and made for your eyes only. Now what are you waiting for? Create a new note and have fun!";
pub const WELCOMING_NOTE_CONTENT: &str = "
{
    \"type\": \"doc\",
    \"content\": [
        {
            \"type\": \"heading\",
            \"attrs\": {
                \"textAlign\": \"left\",
                \"level\": 1
            },
            \"content\": [
                {
                    \"type\": \"text\",
                    \"text\": \"What is Treedome?\"
                }
            ]
        },
        {
            \"type\": \"paragraph\",
            \"attrs\": {
                \"textAlign\": \"left\"
            },
            \"content\": [
                {
                    \"type\": \"text\",
                    \"text\": \"As you can see, it's a note taking desktop application. Everything that you've typed here will be stored in your hard disk \"
                },
                {
                    \"type\": \"text\",
                    \"marks\": [
                        {
                            \"type\": \"bold\"
                        }
                    ],
                    \"text\": \"securely\"
                },
                {
                    \"type\": \"text\",
                    \"text\": \"! Meaning, if you've saved it using this application, no one can reasonably (thousands of years) see what's inside of your notes.\"
                }
            ]
        },
        {
            \"type\": \"paragraph\",
            \"attrs\": {
                \"textAlign\": \"left\"
            }
        },
        {
            \"type\": \"heading\",
            \"attrs\": {
                \"textAlign\": \"left\",
                \"level\": 1
            },
            \"content\": [
                {
                    \"type\": \"text\",
                    \"text\": \"Who is Treedome for?\"
                }
            ]
        },
        {
            \"type\": \"paragraph\",
            \"attrs\": {
                \"textAlign\": \"left\"
            },
            \"content\": [
                {
                    \"type\": \"text\",
                    \"text\": \"Everyone that has a secret really. Which means everyone, ever! Here, let me give you an example.\"
                }
            ]
        },
        {
            \"type\": \"bulletList\",
            \"content\": [
                {
                    \"type\": \"listItem\",
                    \"content\": [
                        {
                            \"type\": \"paragraph\",
                            \"attrs\": {
                                \"textAlign\": \"left\"
                            },
                            \"content\": [
                                {
                                    \"type\": \"text\",
                                    \"text\": \"Teenager that wants a digital diary which their parents shouldn't know about.\"
                                }
                            ]
                        }
                    ]
                },
                {
                    \"type\": \"listItem\",
                    \"content\": [
                        {
                            \"type\": \"paragraph\",
                            \"attrs\": {
                                \"textAlign\": \"left\"
                            },
                            \"content\": [
                                {
                                    \"type\": \"text\",
                                    \"text\": \"Office worker with trade secrets and confidential notes for his eyes only.\"
                                }
                            ]
                        }
                    ]
                },
                {
                    \"type\": \"listItem\",
                    \"content\": [
                        {
                            \"type\": \"paragraph\",
                            \"attrs\": {
                                \"textAlign\": \"left\"
                            },
                            \"content\": [
                                {
                                    \"type\": \"text\",
                                    \"text\": \"Super secret agent with super secret purpose doing for a super secret entity.\"
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            \"type\": \"paragraph\",
            \"attrs\": {
                \"textAlign\": \"left\"
            }
        },
        {
            \"type\": \"heading\",
            \"attrs\": {
                \"textAlign\": \"left\",
                \"level\": 1
            },
            \"content\": [
                {
                    \"type\": \"text\",
                    \"text\": \"Why use Treedome?\"
                }
            ]
        },
        {
            \"type\": \"paragraph\",
            \"attrs\": {
                \"textAlign\": \"left\"
            },
            \"content\": [
                {
                    \"type\": \"text\",
                    \"text\": \"So, you want me to convince you why you should use Treedome?\"
                }
            ]
        },
        {
            \"type\": \"orderedList\",
            \"attrs\": {
                \"start\": 1
            },
            \"content\": [
                {
                    \"type\": \"listItem\",
                    \"content\": [
                        {
                            \"type\": \"paragraph\",
                            \"attrs\": {
                                \"textAlign\": \"left\"
                            },
                            \"content\": [
                                {
                                    \"type\": \"text\",
                                    \"marks\": [
                                        {
                                            \"type\": \"bold\"
                                        }
                                    ],
                                    \"text\": \"It is free!\"
                                },
                                {
                                    \"type\": \"text\",
                                    \"text\": \" You don't need to pay anyone a single cent to use this application in its full glory.\"
                                }
                            ]
                        },
                        {
                            \"type\": \"bulletList\",
                            \"content\": [
                                {
                                    \"type\": \"listItem\",
                                    \"content\": [
                                        {
                                            \"type\": \"paragraph\",
                                            \"attrs\": {
                                                \"textAlign\": \"left\"
                                            },
                                            \"content\": [
                                                {
                                                    \"type\": \"text\",
                                                    \"text\": \"For non-techies, this means no one will sue or take this program away from you.\"
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    \"type\": \"listItem\",
                                    \"content\": [
                                        {
                                            \"type\": \"paragraph\",
                                            \"attrs\": {
                                                \"textAlign\": \"left\"
                                            },
                                            \"content\": [
                                                {
                                                    \"type\": \"text\",
                                                    \"text\": \"For the lawyers/techies, this software is licensed on AGPLv3.\"
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                },
                {
                    \"type\": \"listItem\",
                    \"content\": [
                        {
                            \"type\": \"paragraph\",
                            \"attrs\": {
                                \"textAlign\": \"left\"
                            },
                            \"content\": [
                                {
                                    \"type\": \"text\",
                                    \"marks\": [
                                        {
                                            \"type\": \"bold\"
                                        }
                                    ],
                                    \"text\": \"It is simple!\"
                                },
                                {
                                    \"type\": \"text\",
                                    \"text\": \" Just create a document that's sitting in your hard drive and you're good to go!\"
                                }
                            ]
                        }
                    ]
                },
                {
                    \"type\": \"listItem\",
                    \"content\": [
                        {
                            \"type\": \"paragraph\",
                            \"attrs\": {
                                \"textAlign\": \"left\"
                            },
                            \"content\": [
                                {
                                    \"type\": \"text\",
                                    \"marks\": [
                                        {
                                            \"type\": \"bold\"
                                        }
                                    ],
                                    \"text\": \"It is secure!\"
                                },
                                {
                                    \"type\": \"text\",
                                    \"text\": \" Everything that you put in this note will \"
                                },
                                {
                                    \"type\": \"text\",
                                    \"marks\": [
                                        {
                                            \"type\": \"bold\"
                                        }
                                    ],
                                    \"text\": \"always\"
                                },
                                {
                                    \"type\": \"text\",
                                    \"text\": \" be protected using an cipher based on your password.\"
                                }
                            ]
                        },
                        {
                            \"type\": \"bulletList\",
                            \"content\": [
                                {
                                    \"type\": \"listItem\",
                                    \"content\": [
                                        {
                                            \"type\": \"paragraph\",
                                            \"attrs\": {
                                                \"textAlign\": \"left\"
                                            },
                                            \"content\": [
                                                {
                                                    \"type\": \"text\",
                                                    \"text\": \"For techies out there, we are using scrypt with 2^15 difficulty, use the result of the cipher as the private key for a stream cipher called XChaCha20. More detail on the encryption in the README of this project.\"
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                },
                {
                    \"type\": \"listItem\",
                    \"content\": [
                        {
                            \"type\": \"paragraph\",
                            \"attrs\": {
                                \"textAlign\": \"left\"
                            },
                            \"content\": [
                                {
                                    \"type\": \"text\",
                                    \"marks\": [
                                        {
                                            \"type\": \"bold\"
                                        }
                                    ],
                                    \"text\": \"No logins!\"
                                },
                                {
                                    \"type\": \"text\",
                                    \"text\": \" We are also excited for our capitalistic overlord poaching every bit of private information from the social media. But not this application! Everything is offline and made for your eyes only.\"
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            \"type\": \"paragraph\",
            \"attrs\": {
                \"textAlign\": \"left\"
            },
            \"content\": [
                {
                    \"type\": \"text\",
                    \"text\": \"Now what are you waiting for? Create a new note and have fun!\"
                }
            ]
        }
    ]
}";

use std::{path::Path, sync::Arc};

use crate::error::{CryptoError, TreedomeError};

use self::v1::Cipher;

use super::db::{PlaintextModel, ValidatedSecret};

#[allow(dead_code)]
pub mod v1 {
    use std::{marker::PhantomData, time::Duration};

    use chacha20poly1305::{
        aead::{generic_array::GenericArray, rand_core::RngCore, Aead, Nonce, OsRng},
        AeadCore, AeadInPlace, KeyInit, XChaCha20Poly1305,
    };
    use scrypt::{
        errors::{InvalidOutputLen, InvalidParams},
        scrypt as scrypt_inner, Params as ScryptParams,
    };
    use serde::{de::DeserializeOwned, Deserialize, Serialize};
    use thiserror::Error;

    pub const CURRENT_VERSION_TAG: &str = "v1";
    pub const HEADER_VERSION_TAG_LENGTH: usize = 5;
    pub const HEADER_NONCE_LENGTH: usize = 24;
    pub const HEADER_DATA_SIZE_LENGTH: usize = 8; // size of a u64

    pub const SCRYPT_KEY_LENGTH: usize = 32; // bytes
    pub const SCRYPT_SALT_LENGTH: usize = 16; // bytes
    pub const SCRYPT_DIFFICULTY: u8 = 15;
    pub const ONE_SECOND: Duration = Duration::from_secs(1);

    pub type CipherResult<T> = Result<T, CipherError>;
    pub type ScryptSalt = [u8; SCRYPT_SALT_LENGTH];
    pub type InternalXChaChaNonce = Nonce<XChaCha20Poly1305>;

    pub struct XChaChaNonce(pub InternalXChaChaNonce);

    #[derive(Error, Debug)]
    pub enum CipherError {
        #[error("InnerCryptoError: {0}")]
        InnerCryptoError(String),

        #[error("EncryptedDataError: {0}")]
        EncryptedDataError(String),

        #[error("SerializationError {0}")]
        SerializationError(#[from] ciborium::ser::Error<std::io::Error>),

        #[error("DeserializationError {0}")]
        DeserializationError(#[from] ciborium::de::Error<std::io::Error>),
    }

    impl From<InvalidParams> for CipherError {
        fn from(value: InvalidParams) -> Self {
            Self::InnerCryptoError(value.to_string())
        }
    }

    impl From<InvalidOutputLen> for CipherError {
        fn from(value: InvalidOutputLen) -> Self {
            Self::InnerCryptoError(value.to_string())
        }
    }

    impl From<chacha20poly1305::Error> for CipherError {
        fn from(value: chacha20poly1305::Error) -> Self {
            Self::InnerCryptoError(value.to_string())
        }
    }

    impl From<Nonce<XChaCha20Poly1305>> for XChaChaNonce {
        fn from(value: Nonce<XChaCha20Poly1305>) -> Self {
            Self(value)
        }
    }

    /// A cipher that will live as long as it can
    pub struct Cipher(XChaCha20Poly1305);

    /// Structure of the encrypted data is,
    /// - first 5 bytes are used to indicate the version
    /// - for v0001:
    ///     - 24 bytes after that is used for nonce
    ///     - 8 bytes after that is the length of the data, represented in a u64 (stored in big endian)
    ///     - the rest is the data itself
    ///
    /// I made this in the middle of the night after hours of experimentation, please help.
    #[derive(Serialize, Deserialize)]
    pub struct EncryptedData<T> {
        _danny: PhantomData<T>,
        nonce: XChaChaNonce,
        data: Vec<u8>,
    }

    /// Serde for XChaChaNonce
    struct XChaChaNonceVisitor;
    impl<'de> serde::de::Visitor<'de> for XChaChaNonceVisitor {
        type Value = XChaChaNonce;

        fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
            formatter.write_str("expecting bytes")
        }

        fn visit_bytes<E>(self, v: &[u8]) -> Result<Self::Value, E>
        where
            E: serde::de::Error,
        {
            if v.len() != HEADER_NONCE_LENGTH {
                Err(serde::de::Error::invalid_length(v.len(), &self))?;
            }

            // TODO: make it not clone and directly from_slice, but it's ok though the clone here are
            //       done rarely, so it's very-very negligible
            let eak = InternalXChaChaNonce::clone_from_slice(v);

            Ok(XChaChaNonce(eak))
        }
    }

    impl Serialize for XChaChaNonce {
        fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
        where
            S: serde::Serializer,
        {
            serializer.serialize_bytes(&self.0)
        }
    }

    impl<'de> Deserialize<'de> for XChaChaNonce {
        fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
        where
            D: serde::Deserializer<'de>,
        {
            deserializer.deserialize_bytes(XChaChaNonceVisitor)
        }
    }

    impl Cipher {
        /// Create a new reusable cipher.
        /// The usecase is for application that encrypts the data inside it with a user generated password.
        /// Please make sure that your password is strong enough, for example by using a password checking crate such as zxcvbn.
        pub fn try_new(salt: ScryptSalt, password: &str) -> CipherResult<Self> {
            let key = derive_key_from_password(&salt, password)?;
            let cipher = <XChaCha20Poly1305 as KeyInit>::new(GenericArray::from_slice(&key));
            Ok(Self(cipher))
        }

        /// Create a new valid salt.
        pub fn new_salt() -> ScryptSalt {
            salt_for_password()
        }

        /// Encrypt data
        pub fn encrypt<T: Serialize>(&self, plaintext: &T) -> CipherResult<EncryptedData<T>> {
            let nonce = XChaCha20Poly1305::generate_nonce(&mut OsRng);

            let mut serialized = vec![];
            let _ = ciborium::into_writer(plaintext, &mut serialized)?;
            self.0.encrypt_in_place(&nonce, &[], &mut serialized)?;

            Ok(EncryptedData {
                _danny: PhantomData,
                nonce: nonce.into(),
                data: serialized,
            })
        }

        pub fn decrypt<T: DeserializeOwned>(
            &self,
            encrypted: &EncryptedData<T>,
        ) -> CipherResult<T> {
            let raw_decrypted = self
                .0
                .decrypt(&encrypted.nonce.0, encrypted.data.as_slice())?;

            Ok(ciborium::from_reader(raw_decrypted.as_slice())?)
        }

        pub fn salt_from_vec(v: Vec<u8>) -> CipherResult<ScryptSalt> {
            let salt = v.try_into().map_err(|_| {
                CipherError::EncryptedDataError("salt from db has a bad length".to_string())
            })?;

            Ok(salt)
        }
    }

    /// forked from https://github.com/str4d/rage/blob/bccc4bc9497e0a4542219787ec8183414d1bd9a5/age/src/primitives.rs
    /// Licensed as MIT Copyright (c) 2019 Jack Grigg
    /// Now, GNU Affero Public License version 3 or any later version, Copyright (c) 2024, Solver Orgz
    ///
    /// `scrypt[salt, N](password)`
    /// scrypt from [RFC 7914] with r = 8 and P = 1. N must be a power of 2.
    ///
    /// [RFC 7914]: https://tools.ietf.org/html/rfc7914
    fn derive_key_from_password(
        salt: &[u8],
        password: &str,
    ) -> CipherResult<[u8; SCRYPT_KEY_LENGTH]> {
        let params = ScryptParams::new(SCRYPT_DIFFICULTY, 8, 1, SCRYPT_KEY_LENGTH)?;

        let mut output = [0; SCRYPT_KEY_LENGTH];
        scrypt_inner(password.as_bytes(), salt, &params, &mut output)?;

        Ok(output)
    }

    fn salt_for_password() -> ScryptSalt {
        let mut salt = [0; SCRYPT_SALT_LENGTH];
        OsRng.fill_bytes(&mut salt);
        salt
    }

    /// originally from https://github.com/str4d/rage/blob/bccc4bc9497e0a4542219787ec8183414d1bd9a5/age/src/scrypt.rs
    /// Licensed as MIT Copyright (c) 2019 Jack Grigg
    /// Now, GNU Affero Public License version 3 or any later version, Copyright (c) 2024, Solver Orgz
    ///
    /// USE THIS FOR TESTING ONLY, work factor need to be independent of the computer being used.
    /// This is because we are not creating a new key everytime we are accessing the note.
    ///
    /// Pick an scrypt work factor that will take around 1 second on this device.
    /// Guaranteed to return a valid work factor (less than 64).
    fn target_scrypt_work_factor() -> u8 {
        /// `scrypt[salt, N](password)`
        ///
        /// scrypt from [RFC 7914] with r = 8 and P = 1. N must be a power of 2.
        ///
        /// [RFC 7914]: https://tools.ietf.org/html/rfc7914
        fn scrypt_work_factor_test(
            salt: &[u8],
            log_n: u8,
            password: &str,
        ) -> Result<[u8; 32], InvalidParams> {
            let params = ScryptParams::new(log_n, 8, 1, 32)?;

            let mut output = [0; 32];
            scrypt_inner(password.as_bytes(), salt, &params, &mut output)
                .expect("output is the correct length");
            Ok(output)
        }

        // Time a work factor that should always be fast.
        let mut log_n = 10;

        let duration: Option<Duration> = {
            // Platforms that have a functional SystemTime::now():
            #[cfg(not(all(target_arch = "wasm32", not(target_os = "wasi"))))]
            {
                use std::time::SystemTime;
                let start = SystemTime::now();
                scrypt_work_factor_test(&[], log_n, "").expect("log_n < 64");
                SystemTime::now().duration_since(start).ok()
            }

            // Platforms that can use Performance timer
            #[cfg(all(target_arch = "wasm32", not(target_os = "wasi")))]
            {
                web_sys::window().and_then(|window| {
                    { window.performance() }.map(|performance| {
                        let start = performance.now();
                        derive_key_from_password(&[], log_n, "").expect("log_n < 64");
                        Duration::from_secs_f64((performance.now() - start) / 1_000e0)
                    })
                })
            }

            // Platforms where SystemTime::now() panics:
            #[cfg(all(target_arch = "wasm32", not(target_os = "wasi")))]
            {
                None
            }
        };

        duration
            .map(|mut d| {
                // Use duration as a proxy for CPU usage, which scales linearly with N.
                while d < ONE_SECOND && log_n < 63 {
                    log_n += 1;
                    d *= 2;
                }
                log_n
            })
            .unwrap_or({
                // Couldn't measure, so guess. This is roughly 1 second on a modern machine.
                18
            })
    }

    #[cfg(test)]
    pub mod test {
        use crate::app::{
            crypto::v1::{
                derive_key_from_password, salt_for_password, target_scrypt_work_factor,
                EncryptedData,
            },
            util::speed_test,
        };
        use chacha20poly1305::{
            aead::{generic_array::GenericArray, Aead, AeadCore, KeyInit, OsRng},
            XChaCha20Poly1305,
        };

        use super::Cipher;

        #[test]
        fn test_crypt() {
            let password = "I'd like a huge husky that can actually attack people and not be sick every 3 second";
            let plaintext = "lmao xd".to_string();
            let cipher = speed_test("ciphering", || {
                Cipher::try_new(Cipher::new_salt(), password).unwrap()
            });

            // ACT 1: encrypting data
            let encrypted_data = cipher.encrypt(&plaintext).unwrap();

            // ACT 2: Encoding and Decoding our data, simulating storing and retrieving from disk
            let mut encoded_data = vec![];
            let _ = ciborium::into_writer(&encrypted_data, &mut encoded_data).unwrap();
            let decoded_data =
                ciborium::from_reader::<EncryptedData<String>, _>(encoded_data.as_slice()).unwrap();

            // ACT 3: decrypting our data
            let decrypted_data = cipher.decrypt(&decoded_data).unwrap();

            // ASSERT 1: see if the data decrypted are the same with what we start with
            assert_eq!(decrypted_data, plaintext);
        }

        #[ignore = "testing target's CPU strength is not mandatory"]
        #[test]
        fn test_work_factor() {
            let work_factor = target_scrypt_work_factor();
            assert!(
                work_factor > 10,
                "You have a slow CPU with a work factor of {}, default is 10.",
                work_factor
            )
        }

        #[test]
        fn test_xchacha() {
            // key should be derived from password, because that's how our usecase works
            let plaintext_password = "let them rhythm get into you";

            // this is reusable for the whole lifetime of the application
            let cipher = speed_test("making cipher", || {
                let salt = salt_for_password();
                let hashed_password = derive_key_from_password(&salt, plaintext_password).unwrap();
                XChaCha20Poly1305::new(GenericArray::from_slice(&hashed_password))
            });

            // nonce should be random and NEED to be visible, unencrypted.
            // 192-bits or 24 bytes; unique per message
            let nonce = speed_test("making nonce", || {
                XChaCha20Poly1305::generate_nonce(&mut OsRng)
            });

            let _nonce_not_generic: [u8; 24] = nonce
                .as_slice()
                .try_into()
                .expect("length to be 8 * 24 = 192");

            let ciphertext = speed_test("making cipher text", || {
                cipher
                    .encrypt(&nonce, b"plaintext message".as_ref())
                    .unwrap()
            });

            let plaintext = speed_test("decrypting from ciphertext", || {
                cipher.decrypt(&nonce, ciphertext.as_ref()).unwrap()
            });

            assert_eq!(&plaintext, b"plaintext message");
        }
    }
}

pub fn init_secret<T: AsRef<Path>>(
    model: PlaintextModel,
    path: Option<&T>,
    password: &str,
) -> Result<Arc<Cipher>, TreedomeError> {
    let password_salt = Cipher::salt_from_vec(model.salt).map_err(|e| CryptoError::from(e))?;

    let secret = match path.map(|p| p.as_ref().exists()).unwrap_or(false) {
        true => ValidatedSecret::new_from_unchecked_password(password),
        false => ValidatedSecret::new_from_password(password)?,
    };
    let cipher = Cipher::try_new(password_salt, &secret)
        .map_err(|e| TreedomeError::CryptoError(e.into()))?;
    let secret = Arc::new(cipher);

    Ok(secret)
}

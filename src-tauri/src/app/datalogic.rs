pub mod model;
pub mod note;

use crate::error::TreedomeError;
use async_trait::async_trait;
use model::{NoteRepairParamDatalogic, NoteRepairResultDatalogic};

#[async_trait]
pub trait NoteDatalogic {
    async fn note_repair(
        &self,
        param: NoteRepairParamDatalogic,
    ) -> Result<NoteRepairResultDatalogic, TreedomeError>;
}

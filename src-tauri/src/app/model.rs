#[derive(Debug, serde::Serialize, serde::Deserialize, PartialEq)]
pub struct FoundRange {
    pub start: i64,
    pub end: i64,
    pub before: String,
    pub current: String,
    pub after: String,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct SearchResult {
    pub note_id: String,
    pub title: String,

    /// Use this only for internal use, because rust match by byte index instead of char index.
    pub ranges: Vec<FoundRange>,

    // Filled with every tags in the note
    pub tags: Vec<String>,

    // Text being used for searching
    pub text: String,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct DuplicatedNote {
    pub from: String,
    pub to: String,
}

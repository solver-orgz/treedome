use crate::{
    app::{
        constant::TREEDOME_NEW_EXTENSION,
        db::{EncryptedModel, MetaModel, NoteModel},
        repository::{
            model::{ExportWithCipherParamRepo, ExportWithCipherResultRepo},
            DocumentRepository,
        },
    },
    error::TreedomeError,
};
use async_trait::async_trait;
use futures::TryStreamExt;
use sqlx::sqlite::{SqliteConnectOptions, SqliteJournalMode, SqliteSynchronous};

use super::{init::init_table_sqlite, DocumentRepositorySqlite, TreedomeSqliteConnection, DBV1};

impl DocumentRepositorySqlite {
    pub fn new(conn: TreedomeSqliteConnection) -> Self {
        Self { conn }
    }
}

const VACUUM_V1: &str = "vacuum";

const PLAINTEXT_ONLY_ID: &'static str = "MAIN";

const NOTE_GET_ALL_V1: &str = "SELECT id, data FROM note";
const NOTE_PUT_ONE_V1: &str = "INSERT OR REPLACE INTO note(id, data) VALUES (?, ?)";

const META_GET_ALL_V1: &str = "SELECT id, data FROM meta";
const META_PUT_ONE_V1: &str = "INSERT OR REPLACE INTO meta(id, data) VALUES (?, ?)";

// const PLAINTEXT_GET_ALL_V1: &str = "SELECT id, data FROM plaintext";
const PLAINTEXT_PUT_ONE_V1: &str = "INSERT OR REPLACE INTO plaintext(id, data) VALUES (?, ?)";

#[async_trait]
impl DocumentRepository for DocumentRepositorySqlite {
    async fn document_export_with_cipher(
        &self,
        param: ExportWithCipherParamRepo,
    ) -> Result<ExportWithCipherResultRepo, TreedomeError> {
        if !(param.old_document.exists() && param.old_document.is_file()) {
            return Err(TreedomeError::FailedOpeningDocument(
                "old document path need to point into a valid file".to_string(),
            ));
        }

        let new_path = param.old_document.with_extension(TREEDOME_NEW_EXTENSION);
        if new_path.exists() {
            return Err(TreedomeError::FailedOpeningDocument(format!(
                "new document {:?} already exists, please move it to another folder to proceed",
                new_path
            )));
        }

        // Init Resources
        let old_conn = self.conn.clone();
        let new_conn = {
            let connection_options = SqliteConnectOptions::new()
                .filename(&new_path)
                .create_if_missing(true)
                .journal_mode(SqliteJournalMode::Truncate)
                .synchronous(SqliteSynchronous::Extra)
                .auto_vacuum(sqlx::sqlite::SqliteAutoVacuum::Incremental);

            sqlx::sqlite::SqlitePoolOptions::new()
                .min_connections(5)
                .max_connections(10)
                .connect_with(connection_options)
                .await?
        };

        let _ = init_table_sqlite(&new_conn).await?;

        // Migrate Note
        {
            let mut note_stream = sqlx::query_as::<_, DBV1>(NOTE_GET_ALL_V1).fetch(&old_conn);

            while let Some(note_raw) = note_stream.try_next().await? {
                let note = ciborium::from_reader::<EncryptedModel<NoteModel>, _>(
                    note_raw.data.as_slice(),
                )?
                .into_decrypt(&param.old_secret)?
                .into_encrypt(&param.new_secret)?;

                let mut serialized = vec![];
                let _ = ciborium::into_writer(&note, &mut serialized)?;

                sqlx::query(NOTE_PUT_ONE_V1)
                    .bind(note.get_owned_id())
                    .bind(serialized)
                    .execute(&new_conn)
                    .await?;
            }
        }

        // Migrate Meta
        {
            let mut meta_stream = sqlx::query_as::<_, DBV1>(META_GET_ALL_V1).fetch(&old_conn);

            while let Some(meta_raw) = meta_stream.try_next().await? {
                let meta = ciborium::from_reader::<EncryptedModel<MetaModel>, _>(
                    meta_raw.data.as_slice(),
                )?
                .into_decrypt(&param.old_secret)?
                .into_encrypt(&param.new_secret)?;

                let mut serialized = vec![];
                let _ = ciborium::into_writer(&meta, &mut serialized)?;

                sqlx::query(META_PUT_ONE_V1)
                    .bind(meta.get_owned_id())
                    .bind(serialized)
                    .execute(&new_conn)
                    .await?;
            }
        }

        // Migrate Plaintext
        {
            let mut serialized = vec![];
            let _ = ciborium::into_writer(&param.new_plaintext, &mut serialized)?;

            sqlx::query(PLAINTEXT_PUT_ONE_V1)
                .bind(PLAINTEXT_ONLY_ID)
                .bind(serialized)
                .execute(&new_conn)
                .await?;
        }

        Ok(ExportWithCipherResultRepo {
            new_document: new_path,
        })
    }

    async fn document_compact(&self) -> Result<(), TreedomeError> {
        let _ = sqlx::query(VACUUM_V1).execute(&self.conn).await?;
        Ok(())
    }
}

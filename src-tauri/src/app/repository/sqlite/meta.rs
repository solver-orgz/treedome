use async_trait::async_trait;
use sqlx::Sqlite;

use crate::{
    app::{
        db::{EncryptedModel, MetaModel},
        repository::{
            model::{MetaGetParamRepo, MetaGetResultRepo, MetaPutParamRepo, MetaPutResultRepo},
            sqlite::{MetaRepositorySqlite, TreedomeSqliteConnection, DBV1},
            MetaRepository,
        },
    },
    error::TreedomeError,
};

pub const META_ONLY_ID: &str = "MAIN";
const META_GET_V1: &str = "SELECT id, data FROM meta WHERE id = ?";
const META_PUT_V1: &str = "INSERT OR REPLACE INTO meta(id, data) VALUES (?, ?)";

impl MetaRepositorySqlite {
    pub fn new(conn: TreedomeSqliteConnection) -> Self {
        Self { conn }
    }
}

#[async_trait]
impl MetaRepository for MetaRepositorySqlite {
    async fn meta_put(&self, param: MetaPutParamRepo) -> Result<MetaPutResultRepo, TreedomeError> {
        let mut buffer = vec![];
        ciborium::into_writer(&param.encrypted_data, &mut buffer)?;

        let _ = sqlx::query::<Sqlite>(META_PUT_V1)
            .bind(META_ONLY_ID)
            .bind(buffer)
            .execute(&self.conn)
            .await?;

        Ok(MetaPutResultRepo {})
    }

    async fn meta_get(&self, _: MetaGetParamRepo) -> Result<MetaGetResultRepo, TreedomeError> {
        let query = sqlx::query_as::<Sqlite, DBV1>(META_GET_V1).bind(META_ONLY_ID);

        let result = query
            .fetch_optional(&self.conn)
            .await?
            .map(|v| ciborium::from_reader::<EncryptedModel<MetaModel>, _>(v.data.as_slice()));

        match result {
            Some(v) => Ok(MetaGetResultRepo { model: Some(v?) }),
            None => Ok(MetaGetResultRepo { model: None }),
        }
    }
}

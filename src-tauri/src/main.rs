#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]

pub(crate) mod app;
pub(crate) mod commands;
pub(crate) mod error;
pub(crate) mod tree;

use crate::commands::{
    document_change_password_export, document_close, document_compact, document_open, get_version,
    meta_get, meta_put, note_delete, note_duplicate, note_get, note_get_size, note_put,
    note_search, password_quality_check, spawn_shell_in_document_directory,
};
use app::TreedomeAppWrap;
use commands::greet;
use tracing_subscriber::fmt::format::FmtSpan;

fn main() {
    // To fix not being able to spawn terminal such as alacritty/xterm
    let _ = fix_path_env::fix().unwrap();

    // Configure a custom event formatter
    let format = tracing_subscriber::fmt::format()
        .with_target(false)
        .with_thread_ids(false)
        .with_thread_names(true) // include the name of the current thread
        .with_file(true) // Display source code file paths
        .with_line_number(true) // Display source code line numbers
        .compact(); // use the `Compact` formatting style.

    // Create a `fmt` subscriber that uses our custom event format, and set it
    // as the default.
    #[cfg(debug_assertions)]
    tracing_subscriber::fmt()
        .with_span_events(FmtSpan::NEW | FmtSpan::CLOSE)
        .with_max_level(tracing::Level::TRACE)
        .event_format(format)
        .init();

    // Create a `fmt` subscriber that uses our custom event format, and set it
    // as the default.
    #[cfg(not(debug_assertions))]
    tracing_subscriber::fmt()
        .with_span_events(FmtSpan::NONE)
        .with_max_level(tracing::Level::ERROR)
        .event_format(format)
        .init();

    let app = TreedomeAppWrap::new_empty_document();
    tauri::Builder::default()
        .plugin(tauri_plugin_shell::init())
        .plugin(tauri_plugin_os::init())
        .plugin(tauri_plugin_dialog::init())
        .plugin(tauri_plugin_process::init())
        .plugin(tauri_plugin_fs::init())
        .manage(app)
        // .menu(menu)
        // .on_menu_event(|event| handle_menu_event(event))
        .invoke_handler(tauri::generate_handler![
            greet,
            document_open,
            document_close,
            note_put,
            note_get,
            note_duplicate,
            meta_put,
            meta_get,
            document_compact,
            note_delete,
            note_search,
            note_get_size,
            document_change_password_export,
            password_quality_check,
            spawn_shell_in_document_directory,
            get_version,
        ])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}

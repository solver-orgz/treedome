pub type RootNoteTreeData = Vec<NoteTreeData>;
pub type OpenedNoteTree = Vec<String>;

#[derive(Debug, PartialEq, Clone, serde::Serialize, serde::Deserialize)]
pub struct NoteTreeData {
    pub id: String,
    pub parent: String,
    pub droppable: bool,
    pub text: String,

    pub data: Option<DeepNoteData>,
}

#[derive(Debug, PartialEq, Clone, serde::Serialize, serde::Deserialize)]
pub struct DeepNoteData {
    /// Tags for a note are in the regular note struct and in meta as redundancy.
    /// Treat this one as cache, in case of miscalculation or corruption, we might be able to rebuild this using tags inside notes main struct.
    tags: Option<Vec<String>>,
}

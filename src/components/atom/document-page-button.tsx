import { SegmentedControl } from "@mantine/core"
import { useEffect, useState } from "react"
import { useLocation } from "wouter"
import {
  CREATE_DOCUMENT_PATH,
  OPEN_DOCUMENT_PATH,
  SETTINGS_DOCUMENT_PATH,
} from "../../util/route-path"

const DOCUMENT_BUTTONS_LABEL: TreedomeSegmentedControlItem[] = [
  {
    value: "create_document",
    label: "Create",
  },
  {
    value: "open_document",
    label: "Open",
  },
  {
    value: "settings",
    label: "Settings",
  },
]

export type PageValues = "open_document" | "create_document" | "settings"

export interface DocumentPageButtonProps {
  currentPage: PageValues
}

export interface TreedomeSegmentedControlItem {
  value: PageValues
  label: React.ReactNode
  disabled?: boolean
}

export function DocumentPageButton(props: DocumentPageButtonProps) {
  const [value, setValue] = useState<string>(props.currentPage)
  const [_, setLocation] = useLocation()

  useEffect(() => {
    switch (value as PageValues) {
      case "create_document":
        setLocation(CREATE_DOCUMENT_PATH)
        break

      case "open_document":
        setLocation(OPEN_DOCUMENT_PATH)
        break

      case "settings":
        setLocation(SETTINGS_DOCUMENT_PATH)
        break

      default:
        break
    }
  }, [value])

  return (
    <SegmentedControl
      fullWidth
      value={value}
      onChange={setValue}
      data={DOCUMENT_BUTTONS_LABEL}
    />
  )
}

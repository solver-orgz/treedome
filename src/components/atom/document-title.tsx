import { Title, Text } from "@mantine/core"
import { useState, useEffect } from "react"
import { get_version } from "../../util/treedome-app"

export interface DocumentTitleProps {
  smol?: boolean
}

export const DocumentTitle = (props: DocumentTitleProps) => {
  const [version, setVersion] = useState("")
  useEffect(() => {
    get_version().then((v) => setVersion(v))
  }, [])

  return (
    <>
      <Title ta="center" order={props.smol ? 2 : 1}>
        treedome
        <Text c="dimmed" size="sm">
          {version}
        </Text>
      </Title>
    </>
  )
}

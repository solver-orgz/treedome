import {
  Divider,
  Grid,
  Modal,
  ModalProps,
  SimpleGrid,
  Stack,
  Title,
  Text,
  Button,
  Group,
  NumberInput,
  Checkbox,
  TextInput,
} from "@mantine/core"
import {
  DeepNoteData,
  document_compact,
  note_get_size,
} from "../../util/treedome-app"
import { notifications } from "@mantine/notifications"
import { ChangePasswordModal } from "./change-password-modal"
import { useDisclosure } from "@mantine/hooks"
import { useAtom } from "jotai"
import { DEFAULT_PREFERENCE, preferenceAtom } from "../../util/frontend-command"
import { useForm } from "@mantine/form"
import { NodeModel } from "@minoru/react-dnd-treeview"
import { cloneDeep, size } from "lodash"
import { SaveDocumentParams } from "../organism/document"
import { noteSizeData } from "../../util/markdown-editor"
import { MAX_TIMEOUT_DURATION } from "../atom/document-inactivity"

export interface ConfigurationModalProps extends ModalProps {
  saveDocument: (
    param?: SaveDocumentParams,
  ) => Promise<[NodeModel<DeepNoteData>[], string]>
}

export const ConfigurationModal = (props: ConfigurationModalProps) => {
  const [
    changePasswordNoteOpened,
    { open: changePasswordNoteOpen, close: changePasswordNoteClose },
  ] = useDisclosure(false)
  const [preference, setPreference] = useAtom(preferenceAtom)
  const [noteSize, setNoteSize] = useAtom(noteSizeData)

  const compact = () => {
    document_compact()
      .then(() => {
        notifications.show({
          message: "successfully compacting document",
          autoClose: true,
          withCloseButton: true,
        })
      })
      .catch((e) => {
        console.error(e)
        notifications.show({
          message: "failed compacting document",
          color: "red.7",
          autoClose: true,
          withCloseButton: true,
        })
      })
  }

  const settingsForm = useForm({
    initialValues: {
      open_timeout_ms:
        preference?.open_timeout_ms ?? DEFAULT_PREFERENCE.open_timeout_ms,
      enable_toolbar:
        preference?.enable_toolbar ?? DEFAULT_PREFERENCE.enable_toolbar!,
      enable_floating_toolbar:
        preference?.enable_floating_toolbar ??
        DEFAULT_PREFERENCE.enable_floating_toolbar!,
      document_name:
        preference?.document_name ?? DEFAULT_PREFERENCE.document_name!,
    },
    validate: {
      open_timeout_ms: (v) => {
        if (v !== undefined && v < 30_000) {
          return "Timeout in millisecond must be above 30000"
        }
        if (v !== undefined && v > MAX_TIMEOUT_DURATION) {
          return "Timeout in millisecond must be below 2,147,483,640"
        }
        if (v === undefined) {
          return "Timeout must be filled"
        }
      },
      document_name: (v) => {
        if (v.length > 100) {
          return "must be below 100 characters"
        }
      },
    },
  })

  const calculateAndShowNoteSize = async () => {
    let result = await note_get_size()
    let data = new Map<string, number>()
    for (const datum of result.note_sizes) {
      data.set(datum.note_id, datum.size)
    }

    setNoteSize({
      show: true,
      data: data,
    })
  }

  const hideNoteSize = () => {
    setNoteSize({
      show: false,
      data: new Map(),
    })
  }

  const { saveDocument, ...rest } = props

  return (
    <Modal
      shadow="0" // linux webview went to a crawl when scrolling if shadow is on
      {...rest}
      withCloseButton={false}
      size="lg"
    >
      <ChangePasswordModal
        opened={changePasswordNoteOpened}
        onClose={changePasswordNoteClose}
        centered
      />
      <Stack>
        <Title ta="center" order={2}>
          Generals
        </Title>
        <Divider></Divider>
        <form
          onSubmit={settingsForm.onSubmit((values) => {
            const newPreference = cloneDeep(preference) ?? DEFAULT_PREFERENCE
            newPreference.open_timeout_ms = values.open_timeout_ms
            newPreference.document_name = values.document_name
            newPreference.enable_toolbar = values.enable_toolbar
            newPreference.enable_floating_toolbar =
              values.enable_floating_toolbar
            props
              .saveDocument({
                preference: newPreference,
              })
              .then(() => {
                setPreference(newPreference) // to change the entire app current config
              })
          })}
        >
          <Stack gap="xs">
            <TextInput
              label="Document Title"
              {...settingsForm.getInputProps("document_name")}
            />
            <NumberInput
              label="Idle Timeout in Millisecond"
              allowDecimal={false}
              allowNegative={false}
              allowLeadingZeros={false}
              thousandSeparator=","
              {...settingsForm.getInputProps("open_timeout_ms")}
            />
            <Checkbox
              label="Enable Toolbar"
              {...settingsForm.getInputProps("enable_toolbar", {
                type: "checkbox",
              })}
            />
            <Checkbox
              label="Enable Floating Toolbar"
              {...settingsForm.getInputProps("enable_floating_toolbar", {
                type: "checkbox",
              })}
            />
            <Group justify="flex-end">
              <Button color="blue.7" type="submit">
                save
              </Button>
            </Group>
          </Stack>
        </form>
        <Title ta="center" order={2}>
          Actions
        </Title>
        <Divider></Divider>
        <SimpleGrid cols={1}>
          {/* COMPACT */}
          <ConfigItem title="Optimize Document Size">
            <Button color="blue.7" onClick={compact}>
              run
            </Button>
          </ConfigItem>
          <ConfigItem title="Change Password and Export">
            <Button color="blue.7" onClick={changePasswordNoteOpen}>
              run
            </Button>
          </ConfigItem>
          <ConfigItem title="Show Note Sizes">
            {(noteSize?.show ?? false) ? (
              <Button color="red.7" onClick={hideNoteSize}>
                hide
              </Button>
            ) : (
              <Button color="blue.7" onClick={calculateAndShowNoteSize}>
                calculate
              </Button>
            )}
          </ConfigItem>
        </SimpleGrid>
      </Stack>
    </Modal>
  )
}

interface ConfigItemProps {
  title: string
}

const ConfigItem = (props: React.PropsWithChildren<ConfigItemProps>) => {
  return (
    <Grid justify="space-between" align="center">
      <Grid.Col span={6}>
        <Text>{props.title}</Text>
      </Grid.Col>
      <Grid.Col span="content">{props.children}</Grid.Col>
    </Grid>
  )
}

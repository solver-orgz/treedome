import {
  ActionIcon,
  Button,
  Group,
  Menu,
  Modal,
  TextInput,
  rem,
} from "@mantine/core"
import { AddBox } from "@mui/icons-material"
import { useAtom } from "jotai"
import { currentTreeData } from "../../util/markdown-editor"
import { DeepNoteData, note_put } from "../../util/treedome-app"
import { notifications } from "@mantine/notifications"
import { concat, find } from "lodash"
import { NodeModel } from "@minoru/react-dnd-treeview"
import { useForm } from "@mantine/form"
import { DEFAULT_DOCUMENT_CONTENT_STRING } from "../../util/frontend-command"
import {
  IconCalendarPlus,
  IconChevronDown,
  IconSquarePlus,
} from "@tabler/icons-react"
import { INSTANT_TRANSITION_MODAL } from "../../util/modal"

export interface CreateNoteButtonProps {
  noteID?: string
  createNoteDate: () => void
  open: () => void
  openChild: () => void
  redirectToAnotherNote: (document_id: string, query?: string) => void
}

export function CreateNoteButton(props: CreateNoteButtonProps) {
  return (
    <>
      <Group wrap="nowrap" gap={0}>
        <Button
          style={{
            borderTopRightRadius: 0,
            borderBottomRightRadius: 0,
          }}
          onClick={props.open}
        >
          <AddBox />
        </Button>
        <Menu
          transitionProps={{ transition: "pop" }}
          position="bottom-end"
          withinPortal
        >
          <Menu.Target>
            <ActionIcon
              size={36}
              style={{
                borderTopLeftRadius: 0,
                borderBottomLeftRadius: 0,
              }}
            >
              <IconChevronDown style={{ width: rem(16), height: rem(16) }} />
            </ActionIcon>
          </Menu.Target>
          <Menu.Dropdown>
            <Menu.Item
              leftSection={
                <IconSquarePlus style={{ width: rem(16), height: rem(16) }} />
              }
              onClick={props.open}
            >
              Create Sibling Note
            </Menu.Item>
            <Menu.Item
              leftSection={
                <IconSquarePlus style={{ width: rem(16), height: rem(16) }} />
              }
              onClick={props.openChild}
            >
              Create Child Note
            </Menu.Item>
            <Menu.Item
              leftSection={
                <IconCalendarPlus style={{ width: rem(16), height: rem(16) }} />
              }
              onClick={props.createNoteDate}
            >
              Create Diary Note
            </Menu.Item>
          </Menu.Dropdown>
        </Menu>
      </Group>
    </>
  )
}

export interface CreateNewNoteModalProps {
  opened: boolean
  close: () => void
  makeChildNoteID: boolean
  redirectToAnotherNote: (document_id: string, query?: string) => void
  noteID?: string
  buttonTitle?: string
}

export function CreateNewNoteModal(props: CreateNewNoteModalProps) {
  const [treeNote, setTreeNote] = useAtom(currentTreeData)
  const form = useForm({
    initialValues: {
      title: "",
    },

    validate: {
      title: (v) => {
        if (v.length === 0) {
          return "note title must not be empty"
        }
      },
    },
  })

  const createNewNote = (title: string) => {
    props.close()

    note_put({
      title: title,
      content: DEFAULT_DOCUMENT_CONTENT_STRING,
      text_only: "",
      tags: [],
      create_at: "",
      create_new: true,
    })
      .then((res) => {
        let parentOfCurrentNote = props.noteID ?? "0"

        if (!props.makeChildNoteID) {
          parentOfCurrentNote = String(
            find(treeNote, (v) => {
              return String(v.id) === (props.noteID ?? "")
            })?.parent ?? "0",
          )
        }
        const newNote: NodeModel<DeepNoteData> = {
          id: res.id,
          parent: parentOfCurrentNote,
          text: res.title,
          droppable: true,
        }

        setTreeNote(concat(treeNote, newNote))

        form.reset()
        props.redirectToAnotherNote(res.id)
      })
      .catch((e) => {
        console.error("failed to create new note", e)
        notifications.show({
          message: "failed to create new note",
          color: "red.7",
          autoClose: true,
          withCloseButton: true,
        })
      })
  }

  return (
    <Modal
      shadow="0" // linux webview went to a crawl when scrolling if shadow is on
      {...INSTANT_TRANSITION_MODAL}
      size="md"
      opened={props.opened}
      onClose={props.close}
      centered
      withCloseButton={false}
    >
      <form
        onSubmit={form.onSubmit((values) => {
          createNewNote(values.title)
        })}
      >
        <TextInput {...form.getInputProps("title")} placeholder="note title" />
        <Group justify="right">
          <Button color="blue.7" type="submit" mt="md">
            {props.buttonTitle ??
              (props.makeChildNoteID
                ? "Create Note as Child"
                : "Create Note as Sibling")}
          </Button>
        </Group>
      </form>
    </Modal>
  )
}

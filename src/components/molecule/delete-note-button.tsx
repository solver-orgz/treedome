import { Button, Text } from "@mantine/core"
import { Delete } from "@mui/icons-material"
import { modals } from "@mantine/modals"
import { useAtom } from "jotai"
import { currentTreeData } from "../../util/markdown-editor"
import { concat, filter, find, forEach } from "lodash"
import { note_delete } from "../../util/treedome-app"
import { notifications } from "@mantine/notifications"
import { useLocation } from "wouter"
import { NOTE_PATH } from "../../util/route-path"

export interface DeleteNoteButtonProps {
  note_id?: string
  note_title?: string
}

export function DeleteNoteButton(props: DeleteNoteButtonProps) {
  const [treeNote, setTreeNote] = useAtom(currentTreeData)
  const [_, setLocation] = useLocation()

  const buildNoteTitle = () => {
    if (props.note_title !== undefined) {
      return `"${props.note_title}"`
    }

    return "this note"
  }

  // TODO: optimize this
  const deleteNoteAndItsChildren = () => {
    if (props.note_id === undefined) {
      return
    }

    const currentNoteParent = String(
      find(treeNote, (v) => {
        return String(v.id) === (props.note_id ?? "")
      })?.parent ?? "0",
    )

    // build map of parent => children
    const parentToChildren: Record<string, string[]> = {}
    forEach(treeNote, (v) => {
      parentToChildren[v.parent] = concat(
        parentToChildren[v.parent] ?? [],
        String(v.id),
      )
    })

    // build note ids to delete
    let noteIDsToDelete: Array<string> = [props.note_id]
    let deleteNoteIDsMap: Record<string, boolean> = { [props.note_id]: true }
    let idx = 0
    while (noteIDsToDelete.length > idx) {
      let toSearch = noteIDsToDelete[idx]

      if (toSearch !== undefined) {
        let children = parentToChildren[toSearch] ?? []
        noteIDsToDelete = concat(noteIDsToDelete, children)
        deleteNoteIDsMap[toSearch] = true
        forEach(children, (v) => {
          deleteNoteIDsMap[v] = true
        })
      }

      idx += 1
    }

    if (treeNote.length === noteIDsToDelete.length) {
      notifications.show({
        message: "please do not delete the whole note tree",
        autoClose: true,
        withCloseButton: true,
      })
      return
    }

    // build tree after deleting notes
    // filter out all the deleted noteIDs
    let newTree = filter(treeNote, (v) => {
      return !deleteNoteIDsMap[v.id]
    })

    // get sibling of this note
    let firstAvailableSibling = find(newTree, (v) => {
      return String(v.parent) === currentNoteParent
    })

    // FIXME: check if the note is on the trees or not before deleting
    //        if not, cancel deleting note, create a warning notification, implore to let them open an issue
    let noteIDOpenAfterDelete = String(
      firstAvailableSibling?.id ?? currentNoteParent,
    )

    note_delete({
      note_ids: noteIDsToDelete,
    })
      .then(() => {
        setTreeNote(newTree)
        notifications.show({
          message: `successfully deleted ${props.note_title}, and its children too`,
          autoClose: true,
          withCloseButton: true,
        })
        setLocation(`${NOTE_PATH}/${noteIDOpenAfterDelete}`)
      })
      .catch((reason) => {
        notifications.show({
          message: String(reason),
          color: "red.7",
          autoClose: false,
          withCloseButton: true,
        })
      })
  }

  const openDeleteModal = () => {
    modals.openConfirmModal({
      title: "Delete Note & Children",
      centered: true,
      children: (
        <Text size="sm">
          Are you sure you want to delete {buildNoteTitle()} and its children?
        </Text>
      ),
      labels: { confirm: "Delete with children", cancel: "Cancel" },
      confirmProps: { color: "red" },
      onCancel: () => console.log("Cancel Deleting"),
      onConfirm: () => deleteNoteAndItsChildren(),
    })
  }

  return (
    <Button w="100%" color="red.7" onClick={() => openDeleteModal()}>
      <Delete />
    </Button>
  )
}

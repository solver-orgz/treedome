import { Button } from "@mantine/core"
import { Save } from "@mui/icons-material"

export interface SaveNoteButtonProps {
  onClick: () => void
}
export function SaveNoteButton(props: SaveNoteButtonProps) {
  return (
    <Button w="100%" color="green.7" onClick={props.onClick}>
      <Save />
    </Button>
  )
}

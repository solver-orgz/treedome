import {
  Divider,
  Modal,
  ModalProps,
  Skeleton,
  Stack,
  Table,
  Title,
} from "@mantine/core"
import { INSTANT_TRANSITION_MODAL } from "../../util/modal"
import { fill, sortBy } from "lodash"
import { currentTreeData } from "../../util/markdown-editor"
import { useAtom } from "jotai"
import { useEffect, useState } from "react"

interface TagRow {
  name: string
  notesCount: number
}

export interface TagsModalProps extends ModalProps {}
export function TagsModal(props: TagsModalProps) {
  const [treeData] = useAtom(currentTreeData)
  const [rows, setRows] = useState<TagRow[]>([])

  useEffect(() => {
    let tagsToCount = new Map<string, number>()
    for (let note of treeData) {
      for (let tag of note.data?.tags ?? []) {
        let currentCount = tagsToCount.get(tag)
        if (currentCount === undefined) {
          tagsToCount.set(tag, 1)
        }
        if (currentCount !== undefined) {
          tagsToCount.set(tag, currentCount + 1)
        }
      }
    }

    let result: TagRow[] = []
    tagsToCount.forEach((v, k) => {
      result.push({
        name: k,
        notesCount: v,
      })
    })
    result = sortBy(result, ["name"])

    setRows(result)
  }, [treeData])

  return (
    <Modal
      {...props}
      {...INSTANT_TRANSITION_MODAL}
      shadow="0" // linux webview went to a crawl when scrolling if shadow is on
      centered
      size="md"
      withCloseButton={false}
    >
      <Stack gap="sm">
        <Title ta="center" order={3}>
          Tags
        </Title>

        <Table>
          <Table.Thead>
            <Table.Tr>
              <Table.Th>Name</Table.Th>
              <Table.Th>Notes Count</Table.Th>
            </Table.Tr>
          </Table.Thead>
          <Table.Tbody>
            {rows.map((row) => (
              <Table.Tr key={row.name}>
                <Table.Td>{row.name}</Table.Td>
                <Table.Td>{row.notesCount}</Table.Td>
              </Table.Tr>
            ))}

            {rows.length == 0 && (
              <>
                {fill(Array(5), 0).map((_, i) => (
                  <Table.Tr key={i}>
                    <Table.Td>
                      <Skeleton>Spooky</Skeleton>
                    </Table.Td>
                    <Table.Td>
                      <Skeleton>1234</Skeleton>
                    </Table.Td>
                  </Table.Tr>
                ))}
              </>
            )}
          </Table.Tbody>
        </Table>
      </Stack>
    </Modal>
  )
}

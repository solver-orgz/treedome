import { Route, Switch, useLocation } from "wouter"
import "./app.css"
import {
  CREATE_DOCUMENT_PATH,
  NOTE_PATH,
  OPEN_DOCUMENT_PATH,
  SETTINGS_DOCUMENT_PATH,
} from "../../util/route-path"
import { DocumentEditor } from "./document"
import { useEffect } from "react"
import { CreateDocument } from "./create-document"
import { OpenDocument } from "./open-document"
import { initOsType, osType } from "../../util/markdown-editor"
import { useAtom } from "jotai"
import { SettingsDocument } from "./settings-document"
import { EmojiSearchFuse, initEmojiSearchData } from "../../util/emoji-picker"
import { preferenceAtom } from "../../util/frontend-command"
import { getCurrentWebviewWindow } from "@tauri-apps/api/webviewWindow"
import { listen } from "@tauri-apps/api/event"
import { Menu } from "@tauri-apps/api/menu"

function App() {
  const [, setAtom] = useAtom(osType)
  const [, setEmojiSearchFuse] = useAtom(EmojiSearchFuse)
  const [_, setLocation] = useLocation()
  const [currentPreferenceAtom] = useAtom(preferenceAtom)

  // go to /open-document when the app starts
  useEffect(() => {
    initOsType(setAtom)
    initEmojiSearchData(setEmojiSearchFuse)
    setLocation("/open-document")
  }, [])

  useEffect(() => {
    const appWindow = getCurrentWebviewWindow()
    if (currentPreferenceAtom?.document_name) {
      appWindow.setTitle(`${currentPreferenceAtom.document_name} - treedome`)
    } else {
      appWindow.setTitle(`treedome`)
    }
  }, [currentPreferenceAtom])

  return (
    <Switch>
      <Route path={`${NOTE_PATH}/:document_id/:search_text?`}>
        {(param) => (
          <DocumentEditor
            document_id={param.document_id}
            search_text={param.search_text}
          />
        )}
      </Route>
      <Route path={CREATE_DOCUMENT_PATH} component={CreateDocument} />
      <Route path={OPEN_DOCUMENT_PATH} component={OpenDocument} />
      <Route path={SETTINGS_DOCUMENT_PATH} component={SettingsDocument} />
    </Switch>
  )
}

// Example on how to add context menu that pops on right click
// TODO:
//  - not used yet but functional
//  - option menu replaces the current menu
const RightClickMenu: React.FC<React.PropsWithChildren> = (props) => {
  const MENU_ITEMS = [
    { id: "ctx_option1", text: "Option 1" },
    { id: "ctx_option2", text: "Option 2" },
  ]

  const menuPromise = Menu.new({
    items: MENU_ITEMS,
  })

  async function clickHandler(event: React.MouseEvent) {
    event.preventDefault()

    const menu = await menuPromise

    menu.popup()
  }

  useEffect(() => {
    const unlistenPromise = listen<string>("menu-event", (event) => {
      if (!event.payload.startsWith("ctx")) return
      switch (event.payload) {
        default:
          console.log("Unimplemented application menu id:", event.payload)
      }
    })

    return () => {
      unlistenPromise.then((unlisten) => unlisten())
    }
  }, [])

  return <div onContextMenu={clickHandler}>{props.children}</div>
}

export default App

import "./app.css"

import { TextEditor } from "../atom/text-editor"
import { useAtom } from "jotai"
import {
  currentTreeData,
  editorContent,
  editorIsChanged,
  editorRef,
  handleShortcuts,
  openNoteData,
  treedome_decode,
  treedome_encode,
  uniqueTagsFromTreeData,
} from "../../util/markdown-editor"
import { useEffect, useState } from "react"
import {
  DeepNoteData,
  PreferenceModel,
  meta_put,
  note_duplicate,
  note_get,
  spawn_shell_in_document_directory,
} from "../../util/treedome-app"
import {
  Button,
  Container,
  Divider,
  Grid,
  Input,
  Skeleton,
  Stack,
  TagsInput,
  Text,
  Tooltip,
} from "@mantine/core"
import {
  buildActionFromTreeData,
  buildChildren,
  buildNoteParentList,
  buildNoteParentListBreadcrumb,
  NoteTree,
  sortChildrenRecursively,
  spotlightFilter,
} from "../atom/note-tree"
import { useLocation } from "wouter"
import { NOTE_PATH } from "../../util/route-path"
import { notifications } from "@mantine/notifications"
import {
  cloneDeep,
  concat,
  difference,
  forEach,
  has,
  keyBy,
  keys,
  map,
  merge,
  reduce,
} from "lodash"
import { useDisclosure, useViewportSize } from "@mantine/hooks"
import { DeleteNoteButton } from "../molecule/delete-note-button"
import {
  CreateNewNoteModal,
  CreateNoteButton,
} from "../molecule/create-note-button"
import { EscapeMenuModal } from "../molecule/escape-menu-modal"
import {
  createDateNote,
  DEFAULT_DOCUMENT_CONTENT_STRING,
  DEFAULT_DOCUMENT_CONTENT,
  saveNoteAndTrees,
  get_terminal_command,
  get_tree_children,
  preferenceAtom,
  DEFAULT_PREFERENCE,
  saveMetaOnly,
} from "../../util/frontend-command"
import {
  ContentCopy,
  Menu,
  SwapVert,
  UnfoldLess,
  UnfoldMore,
} from "@mui/icons-material"
import { Spotlight } from "@mantine/spotlight"
import { SearchModal } from "../molecule/search-modal"
import { HelpModal } from "../molecule/help-modal"
import { JSONContent } from "@tiptap/react"
import { SaveNoteButton } from "../molecule/save-note-button"
import { SaveConfirmationModal } from "../molecule/save-confirmation-modal"
import { NodeModel } from "@minoru/react-dnd-treeview"
import { SpotlightActions } from "@mantine/spotlight/lib/Spotlight"
import { BreadcrumbNotePath } from "../atom/breadcrumb-note-path"
import {
  Container as CContainer,
  Section as CSection, // C here means "ColumnResizer"-Section
  Bar as CBar,
} from "@column-resizer/react"
import { DocumentInactivity } from "../atom/document-inactivity"
import { EmojiPickerButton } from "../molecule/emoji-picker-button"
import { DateTime } from "luxon"
import { spotlightFuse } from "../../util/note-search"
import { EmojiRegx } from "../../util/emoji-picker"
import Fuse from "fuse.js"
import { TagsModal } from "../molecule/tags-modal"

export interface DocumentEditorProps {
  document_id: string
  search_text?: string
}

export function DocumentEditor(props: DocumentEditorProps) {
  const [_, setLocation] = useLocation()

  const [isLoaded, setIsLoaded] = useState(false)
  const [canLoad, setCanLoad] = useState(true) // acting like a mutex, so we will wait until the note is loaded to load another one. Don't use isLoaded because it's used by the UI.
  const [currentNoteID, setCurrentNoteID] = useState<string | undefined>()
  const [title, setTitleText] = useState("")
  const [createAt, setCreateAtText] = useState("")
  const [updateAt, setUpdateAtText] = useState("")
  const [tags, setTags] = useState<string[]>([])
  const [currentPreferenceAtom] = useAtom(preferenceAtom)

  const [treeData, setTreeData] = useAtom(currentTreeData)
  const [currentOpenNoteData, setCurrentOpenNoteData] = useAtom(openNoteData)
  const [fuseActions, setFuseActions] = useAtom(spotlightFuse)
  const [spotlightActions, setSpotlightActions] = useState<SpotlightActions[]>(
    [],
  )

  // For Modals
  const [
    createNewNoteOpened,
    { open: createNewNoteOpen, close: createNewNoteClose },
  ] = useDisclosure(false)
  const [
    createNewNoteAsChildOpened,
    { open: createNewNoteAsChildOpen, close: createNewNoteAsChildClose },
  ] = useDisclosure(false)
  const [
    escapeMenuOpened,
    { close: escapeMenuClose, toggle: escapeMenuToggle },
  ] = useDisclosure(false)
  const [searchOpened, { close: searchClose, toggle: searchToggle }] =
    useDisclosure(false)
  const [
    helpModalOpened,
    { open: helpModalOpen, close: helpModalClose, toggle: helpModalToggle },
  ] = useDisclosure(false)
  const [
    saveConfirmationOpened,
    { open: saveConfirmationOpen, close: saveConfirmationClose },
  ] = useDisclosure(false)
  const [tagsModalOpened, { open: tagsModalOpen, close: tagsModalClose }] =
    useDisclosure(false)

  const [editor] = useAtom(editorRef)
  const [, setContent] = useAtom(editorContent)
  const [isChanged, setIsChanged] = useAtom(editorIsChanged)
  const setChangedOnce = () => {
    if (!isChanged) {
      setIsChanged(true)
    }
  }

  // For Editor Resize
  const { width: screenWidth } = useViewportSize()

  useEffect(() => {
    editor?.on("update", setChangedOnce)

    return () => {
      editor?.off("update", setChangedOnce)
    }
  }, [editor])

  const reloadCurrentNote = async (stored_id: string) => {
    if (!canLoad) {
      return
    }

    setIsChanged(false)
    setIsLoaded(false)

    let res = await note_get({
      ids: [stored_id],
    })

    if (res.result === undefined || res.result.length === 0) {
      notifications.show({
        message: "note not found",
        color: "red.7",
        autoClose: true,
        withCloseButton: true,
      })
    }
    let note_result = res.result[0]

    setCurrentNoteID(stored_id)
    setTitleText(note_result.data.title ?? "")
    setTags(note_result.data.tags ?? [])
    setUpdateAtText(note_result.data.update_at ?? "")
    setCreateAtText(note_result.data.create_at ?? "")

    let value =
      treedome_decode<JSONContent>(
        note_result.data.content ?? DEFAULT_DOCUMENT_CONTENT_STRING,
      ) ?? DEFAULT_DOCUMENT_CONTENT

    setContent(value)

    setIsLoaded(true)
    setCanLoad(true)
  }

  const redirectToAnotherNote = (document_id: string, query?: string) => {
    if (isChanged) {
      saveConfirmationOpen()
      return
    }

    setLocation(NOTE_PATH + `/${document_id}/${query ?? ""}`)
  }

  useEffect(() => {
    reloadCurrentNote(props.document_id).catch((e) => {
      console.error("failed loading note from disk", e)
      notifications.show({
        message: "failed loading note from disk",
        color: "red.7",
        autoClose: true,
        withCloseButton: true,
      })
    })
  }, [props.document_id, canLoad])

  useEffect(() => {
    saveMetaOnly(
      props.document_id,
      treeData,
      currentOpenNoteData,
      currentPreferenceAtom ?? DEFAULT_PREFERENCE,
    )
  }, [props.document_id, treeData, currentOpenNoteData, currentPreferenceAtom])

  useEffect(() => {
    setSpotlightActions(
      buildActionFromTreeData(treeData, redirectToAnotherNote),
    )
  }, [treeData, isChanged])

  useEffect(() => {
    let currentParent = ""
    forEach(treeData, (noteData) => {
      if (String(noteData.id) === props.document_id) {
        currentParent = String(noteData.parent)
      }
    })

    // FIXME: cache currentlyOpen to an atomic hook
    let currentlyOpenSet: Set<string> = new Set()
    forEach(currentOpenNoteData, (v) => {
      currentlyOpenSet.add(String(v))
    })

    let parents = buildNoteParentList(treeData, currentParent)
    let parentToOpen: string[] = []
    forEach(parents, (parent) => {
      if (!currentlyOpenSet.has(parent)) {
        parentToOpen = concat(parentToOpen, parent)
      }
    })

    setCurrentOpenNoteData(concat(currentOpenNoteData, parentToOpen))
  }, [props.document_id, treeData])

  const normalizedString = (raw: string): string => {
    let label = (raw as string).replaceAll(EmojiRegx, "")
    label = label.replaceAll("➜", "")
    label = label
      .trim()
      .split(/[\s,\t,\n]+/)
      .join(" ")

    return label
  }

  useEffect(() => {
    const normalizedActions = map(cloneDeep(spotlightActions), (v) => {
      if (has(v, "label")) {
        v.label = normalizedString(String(v.label ?? ""))
      }

      if (has(v, "keywords")) {
        v.keywords = normalizedString(String(v.keywords ?? ""))
      }

      return v
    })

    const fuse = new Fuse(normalizedActions, {
      keys: ["label", "keywords"],
      shouldSort: true,
      ignoreLocation: true,
      minMatchCharLength: 2,
      threshold: 0.6,
    })

    setFuseActions(fuse)
  }, [spotlightActions])

  const sortCurrentNoteAndChildren = () => {
    setCanLoad(false)
    setIsLoaded(false)

    let newTree = sortChildrenRecursively(treeData, props.document_id)
    setTreeData(newTree)

    setCanLoad(true)
    setIsLoaded(true)
  }

  const duplicateNoteIDs = async () => {
    try {
      setIsLoaded(false)
      let childrenAndParent = get_tree_children(treeData, props.document_id)

      let duplicated = await note_duplicate({
        ids: childrenAndParent,
      })
      let duplicatedMap: Record<string, string> = reduce(
        duplicated.result,
        (prev: Record<string, string>, v) => {
          prev[v.from] = v.to
          return prev
        },
        {},
      )

      const keyToData: Record<string, NodeModel> = {}
      forEach(treeData, (v) => {
        keyToData[v.id] = v
      })

      let newTreeData: NodeModel<DeepNoteData>[] = []
      forEach(duplicated.result, (v) => {
        // transform all old id (own and parent) using duplicateMap
        let oldNode = keyToData[v.from]
        let newNode: NodeModel<DeepNoteData> = {
          id: duplicatedMap[v.from],
          parent: duplicatedMap[oldNode.parent] ?? oldNode.parent,
          text: oldNode.text,
          droppable: true,
        }

        newTreeData = concat(newTreeData, newNode)
      })

      let newTree = concat(treeData, newTreeData)
      setTreeData(concat(treeData, newTreeData))
      await meta_put({
        tree: newTree,
        opened_notes: currentOpenNoteData,
        last_opened_note_id: props.document_id,
        preference: currentPreferenceAtom ?? DEFAULT_PREFERENCE,
      })

      notifications.show({
        message: "successfully duplicating notes",
        color: "green.7",
        autoClose: 750,
        withCloseButton: true,
      })

      setIsLoaded(true)
    } catch (e) {
      console.error("failed duplicating your notes", e)
      notifications.show({
        message: "failed duplicating your notes",
        color: "red.7",
        withCloseButton: true,
      })

      setIsLoaded(true)
    }
  }

  const expandNoteAndChildrenRecursively = () => {
    let openNoteMap = keyBy(
      [...currentOpenNoteData, props.document_id],
      (o) => o,
    )
    let childrenMap = keyBy(buildChildren(treeData, props.document_id), "id")
    let merged = keys(merge(openNoteMap, childrenMap))

    setCurrentOpenNoteData(merged)
  }

  const collapseNoteAndChildrenRecursively = () => {
    let childrenMap = buildChildren(treeData, props.document_id)
    let merged = difference(currentOpenNoteData, [
      ...map(childrenMap, (o) => String(o.id)),
      props.document_id,
    ])

    setCurrentOpenNoteData(merged)
  }

  const saveDocument = async (param?: SaveDocumentParams) => {
    return await saveNoteAndTrees(
      true,
      props.document_id,
      title,
      treedome_encode(editor?.getJSON() ?? DEFAULT_DOCUMENT_CONTENT),
      editor?.getText() ?? "",
      treeData,
      currentOpenNoteData,
      tags,
      param?.preference ?? currentPreferenceAtom ?? DEFAULT_PREFERENCE,
      createAt,
      () => setIsChanged(false),
    )
  }

  const createNoteDate = async () => {
    setCanLoad(false)
    setIsLoaded(false)

    const [dateID, newTree] = await createDateNote(treeData)
    setTreeData(newTree)

    setCanLoad(true)
    setIsLoaded(true)

    setLocation(NOTE_PATH + `/${dateID}`)
  }

  const buildHumanDateTimeFromRfc3339 = (
    datetime_rfc3339: string,
    relative: boolean,
  ): string => {
    if (datetime_rfc3339 === "") {
      return "not set"
    }

    const dt = DateTime.fromISO(datetime_rfc3339)

    if (relative) {
      return dt.toRelative() ?? dt.toLocaleString(DateTime.DATETIME_MED)
    }

    return dt.toLocaleString(DateTime.DATETIME_MED)
  }

  return (
    <>
      <DocumentInactivity saveDocument={saveDocument} />
      <Spotlight
        actions={spotlightActions} // make this done from query instead
        disabled={!isLoaded}
        shortcut="mod + shift + p"
        nothingFound="Here be dragons..."
        filter={spotlightFilter(fuseActions)}
        triggerOnContentEditable
        scrollable
        highlightQuery
        clearQueryOnClose
      />

      <EscapeMenuModal
        opened={escapeMenuOpened}
        tagsModalOpen={tagsModalOpen}
        onClose={escapeMenuClose}
        searchToggle={searchToggle}
        helpModalOpen={helpModalOpen}
        saveDocument={saveDocument}
      />
      <CreateNewNoteModal
        redirectToAnotherNote={redirectToAnotherNote}
        makeChildNoteID={false}
        opened={createNewNoteOpened}
        close={createNewNoteClose}
        noteID={props.document_id}
      />
      <CreateNewNoteModal
        redirectToAnotherNote={redirectToAnotherNote}
        makeChildNoteID={true}
        opened={createNewNoteAsChildOpened}
        close={createNewNoteAsChildClose}
        noteID={props.document_id}
      />
      <SearchModal
        opened={searchOpened}
        onClose={searchClose}
        redirectNote={redirectToAnotherNote}
      />
      <HelpModal opened={helpModalOpened} onClose={helpModalClose} />
      <TagsModal opened={tagsModalOpened} onClose={tagsModalClose} />
      <SaveConfirmationModal
        opened={saveConfirmationOpened}
        close={saveConfirmationClose}
      />

      <Container
        size="100%"
        style={{ paddingTop: "1em", paddingBottom: "1em", height: "100vh" }}
        onKeyDown={(event) => {
          handleShortcuts(event, {
            save: async () => {
              const [newTree, title] = await saveDocument()
              setTreeData(newTree)
              setTitleText(title)
            },
            createNote: async () => createNewNoteOpen(),
            createNoteAsChild: async () => createNewNoteAsChildOpen(),
            createNoteDate: createNoteDate,
            escapeMenu: async () => {
              if (
                !(
                  createNewNoteOpened ||
                  createNewNoteAsChildOpened ||
                  searchOpened ||
                  helpModalOpened
                )
              ) {
                escapeMenuToggle()
              }
            },
            searchMenu: async () => {
              if (
                !(
                  createNewNoteOpened ||
                  createNewNoteAsChildOpened ||
                  escapeMenuOpened ||
                  helpModalOpened
                )
              ) {
                searchToggle()
              }
            },
            helpMenu: async () => {
              if (
                !(
                  createNewNoteOpened ||
                  createNewNoteAsChildOpened ||
                  escapeMenuOpened ||
                  searchOpened
                )
              ) {
                helpModalToggle()
              }
            },
            spawnNewTerminal: async () => {
              try {
                await spawn_shell_in_document_directory({
                  command: get_terminal_command(),
                })
              } catch (e) {
                notifications.show({
                  message: `failed opening terminal: ${e}`,
                  color: "red.7",
                  autoClose: true,
                  withCloseButton: true,
                })
              }
            },
          })
        }}
        tabIndex={0} // yes, this is important so onKeyDown can work anywhere you're focused on.
      >
        <CContainer>
          <CSection
            defaultSize={1} // yes, we default to 1 because first two frame of `screenWidth` is 0. Value of 1 will turn this section to
            minSize={(screenWidth * 3.0) / 12.0}
            style={{ padding: "0.5em" }}
          >
            <Skeleton visible={!isLoaded}>
              <Stack
                gap="0"
                h="96vh"
                bg="dark.8"
                style={{ borderRadius: "0.5em" }}
              >
                <Button.Group
                  style={{
                    overflowX: "auto",
                    overflowY: "hidden",
                  }}
                  p="sm"
                >
                  <Tooltip label="Open Menu">
                    <Button
                      w="100%"
                      miw="4.25em"
                      color="dark.1"
                      onClick={() => escapeMenuToggle()}
                    >
                      <Menu />
                    </Button>
                  </Tooltip>
                  <Tooltip
                    label="Expand Note & Children"
                    onClick={() => expandNoteAndChildrenRecursively()}
                  >
                    <Button w="100%" miw="4.25em" color="dark.1">
                      <UnfoldMore />
                    </Button>
                  </Tooltip>
                  <Tooltip
                    label="Collapse Note & Children"
                    onClick={() => collapseNoteAndChildrenRecursively()}
                  >
                    <Button w="100%" miw="4.25em" color="dark.1">
                      <UnfoldLess />
                    </Button>
                  </Tooltip>
                  <Tooltip
                    label="Sort Note"
                    onClick={() => sortCurrentNoteAndChildren()}
                  >
                    <Button w="100%" miw="4.25em" color="dark.1">
                      <SwapVert />
                    </Button>
                  </Tooltip>
                  <Tooltip
                    label="Duplicate Note"
                    onClick={() => duplicateNoteIDs()}
                  >
                    <Button w="100%" miw="4.25em" color="dark.1">
                      <ContentCopy />
                    </Button>
                  </Tooltip>
                </Button.Group>
                <Divider my="xs" mt={0} />
                <NoteTree
                  isLoaded={isLoaded}
                  currentOpenID={props.document_id}
                  onNoteClick={(id) => redirectToAnotherNote(id)}
                />
              </Stack>
            </Skeleton>
          </CSection>

          <CBar
            size={4}
            className="appear-on-hover"
            style={{
              background: "gray",
              cursor: "pointer",
              // marginLeft: "0.5em",
              // marginRight: "0.5em",
              marginBottom: "2em",
              marginTop: "2em",
            }}
          />

          <CSection minSize={500} style={{ padding: "0.5em" }}>
            <Skeleton visible={!isLoaded}>
              {/* FIXME: oh my god the height thingy makes me so mad */}
              <Stack
                justify="flex-start"
                gap="10px"
                align="stretch"
                style={{ height: "96vh" }}
              >
                <Grid gutter="xs">
                  <Grid.Col span="auto">
                    <Input
                      placeholder="Title"
                      type="text"
                      styles={{
                        input: {
                          border: 0,
                        },
                      }}
                      style={{ minWidth: "100%" }}
                      value={title}
                      onChange={(e: any) => {
                        setTitleText(e.target.value)
                        setIsChanged(true)
                      }}
                    />
                  </Grid.Col>
                  <Tooltip label="Add Emoji">
                    <Grid.Col span="content">
                      <EmojiPickerButton
                        onChangeEmoji={(emoji) => {
                          setTitleText(`${emoji} ${title}`)
                          setIsChanged(true)
                        }}
                      />
                    </Grid.Col>
                  </Tooltip>
                  <Tooltip label="Create New Note">
                    <Grid.Col span="content">
                      <CreateNoteButton
                        open={createNewNoteOpen}
                        openChild={createNewNoteAsChildOpen}
                        redirectToAnotherNote={redirectToAnotherNote}
                        noteID={currentNoteID}
                        createNoteDate={createNoteDate}
                      />
                    </Grid.Col>
                  </Tooltip>
                  <Tooltip label="Delete Current Note">
                    <Grid.Col span="content">
                      <DeleteNoteButton
                        note_id={currentNoteID}
                        note_title={title}
                      />
                    </Grid.Col>
                  </Tooltip>
                  {isChanged && (
                    <Tooltip label="Save Button">
                      <Grid.Col span="content">
                        <SaveNoteButton
                          onClick={async () => {
                            const [newTree, title] = await saveDocument()
                            setTreeData(newTree)
                            setTitleText(title)
                          }}
                        />
                      </Grid.Col>
                    </Tooltip>
                  )}
                </Grid>
                <TagsInput
                  placeholder="Tags - Enter to Add Tags"
                  type="text"
                  styles={{
                    input: {
                      border: 0,
                    },
                  }}
                  style={{ minWidth: "100%" }}
                  data={uniqueTagsFromTreeData(treeData)}
                  value={tags}
                  onChange={(e) => {
                    setTags(e)
                    setIsChanged(true)
                  }}
                />

                <BreadcrumbNotePath
                  notePathItems={buildNoteParentListBreadcrumb(
                    treeData,
                    currentNoteID ?? "",
                  )}
                />

                {/* YEAH THIS IS IT, THE TEXT EDITOR! */}
                <TextEditor search_text={props.search_text} />

                <Grid gutter="">
                  <Grid.Col span="content">
                    <Tooltip inline label="document title">
                      <Text span size="xs">
                        {currentPreferenceAtom?.document_name ??
                          DEFAULT_PREFERENCE.document_name!}
                      </Text>
                    </Tooltip>
                  </Grid.Col>
                  <Grid.Col span="auto" />
                  <Grid.Col span="content">
                    <Tooltip inline label="created at">
                      <Text span size="xs">
                        {buildHumanDateTimeFromRfc3339(createAt, false)}
                      </Text>
                    </Tooltip>
                    {" ⸺ "}
                    <Tooltip
                      inline
                      label={`last saved ${buildHumanDateTimeFromRfc3339(createAt, false)}`}
                    >
                      <Text span size="xs">
                        {buildHumanDateTimeFromRfc3339(updateAt, true)}
                      </Text>
                    </Tooltip>
                  </Grid.Col>
                </Grid>
              </Stack>
            </Skeleton>
          </CSection>
        </CContainer>
      </Container>
    </>
  )
}

export interface SaveDocumentParams {
  preference?: PreferenceModel
}

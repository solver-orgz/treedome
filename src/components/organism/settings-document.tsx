import {
  TextInput,
  Button,
  Stack,
} from "@mantine/core"
import { useForm } from "@mantine/form"
import { useEffect } from "react"
import {
  get_terminal_command,
  set_terminal_command,
} from "../../util/frontend-command"
import { HomeComponent } from "../molecule/home-component"

export const SettingsDocument: React.FC<{}> = () => {
  const form = useForm({
    initialValues: {
      terminalCommand: "",
    },
  })

  useEffect(() => {
    form.setFieldValue("terminalCommand", get_terminal_command())
  }, [])

  return (
    <HomeComponent currentPage="settings">
      <form
        onSubmit={form.onSubmit((v) => {
          set_terminal_command(v.terminalCommand)
        })}
      >
        <Stack align="stretch">
          <TextInput
            {...form.getInputProps("terminalCommand")}
            label="Open Terminal Command"
            placeholder={'alacritty --working-directory "{directory}"'}
          />
          <Button type="submit">submit</Button>
        </Stack>
      </form>
    </HomeComponent>
  )
}

import React from "react"
import App from "./components/organism/app"
import { createRoot } from "react-dom/client"
import { ActionIcon, Button, Input, MantineProvider } from "@mantine/core"
import { ModalsProvider } from "@mantine/modals"
import { Notifications } from "@mantine/notifications"
import "@fontsource/noto-sans"
import "@fontsource/noto-sans-mono"
import "@mantine/notifications/styles.css"
import "@mantine/core/styles.css"
import "@mantine/tiptap/styles.css"
import "@mantine/spotlight/styles.css"
import "./style.css"

const domNode = document.getElementById("root") as HTMLElement
const root = createRoot(domNode)
root.render(
  <React.StrictMode>
    <MantineProvider
      forceColorScheme="dark"
      theme={{
        components: {
          Input: Input.extend({
            styles: {
              input: {
                border: 0,
              },
            },
          }),

          Button: Button.extend({
            defaultProps: {
              variant: "light",
            },
          }),

          ActionIcon: ActionIcon.extend({
            defaultProps: {
              variant: "light",
            },
          }),
        },
        fontFamily: "Noto Sans",
        fontFamilyMonospace: "Noto Sans Mono",
        spacing: {
          xs: "1rem",
          sm: "1.2rem",
          md: "1.8rem",
          lg: "2.2rem",
          xl: "2.8rem",
        },
        colors: {
          // These are the old mantine v6 dark colors found here: https://v6.mantine.dev/guides/dark-theme/
          // We can use the new v7 dark colors but I dislike it because it's too light for my taste
          dark: [
            "#C1C2C5",
            "#A6A7AB",
            "#909296",
            "#5C5F66",
            "#373A40",
            "#2C2E33",
            "#25262B",
            "#1A1B1E",
            "#141517",
            "#101113",
          ],
        },
      }}
    >
      <Notifications />
      <ModalsProvider>
        <App />
      </ModalsProvider>
    </MantineProvider>
  </React.StrictMode>,
)

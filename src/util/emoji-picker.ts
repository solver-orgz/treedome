import emojiData, { EmojiMartData } from "@emoji-mart/data"
import Fuse from "fuse.js"
import { atom } from "jotai"
import { values, first } from "lodash"
import emojiRegex from "emoji-regex"

export const EmojiRegx = emojiRegex()

export interface FuseEmojiSearchData {
  emoji: string // unicode, ready to present to user such as 🇮🇩
  searchable: string[] // all searchable query
  label: string // label being presented to user
}
export const EmojiSearchFuse = atom<Fuse<FuseEmojiSearchData>>(new Fuse([]))
export const initEmojiSearchData = (
  setEmojiSearchFuse: (value: Fuse<FuseEmojiSearchData>) => void,
) => {
  let castedEmojiData = emojiData as EmojiMartData
  let transformedEmojiData = values(castedEmojiData.emojis).map((e) => {
    let emoji = first(e.skins)?.native ?? ""
    let name = e.name ?? ""

    return {
      emoji: emoji,
      searchable: [e.name, ...e.keywords],
      label: name,
    }
  })

  const fuse = new Fuse(transformedEmojiData, {
    keys: ["searchable"],
    shouldSort: true,
    ignoreLocation: true,
    minMatchCharLength: 2,
    threshold: 0.25,
  })

  setEmojiSearchFuse(fuse)
}

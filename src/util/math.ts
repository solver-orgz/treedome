export function HumanFileSize(sizeInByte: number): string {
  var i =
    sizeInByte == 0 ? 0 : Math.floor(Math.log(sizeInByte) / Math.log(1024))
  return (
    +(sizeInByte / Math.pow(1024, i)).toFixed(2) * 1 +
    " " +
    ["B", "kB", "MB", "GB", "TB"][i]
  )
}

import { atom } from "jotai"
import { SearchResult } from "./treedome-app"
import Fuse from "fuse.js"
import { SpotlightActions } from "@mantine/spotlight/lib/Spotlight"

export const searchResult = atom<SearchResult[]>([])
export enum SearchingStateEnum {
  InProgress,
  Done,
}
export const searchingState = atom<SearchingStateEnum>(SearchingStateEnum.Done)
export const spotlightFuse = atom<Fuse<SpotlightActions>>(new Fuse([]))

export const NOTE_PATH = "/note" // :note
export const CREATE_DOCUMENT_PATH = "/create-document" // this page is for opening up a new/existing notes
export const OPEN_DOCUMENT_PATH = "/open-document" // this page is for opening up a existing notes
export const SETTINGS_DOCUMENT_PATH = "/settings-document" // this page is for opening up a existing notes
